# Action Scheduler module

This module is based on the Action Scheduler library ([source](https://github.com/woocommerce/action-scheduler/) and [documentation](https://actionscheduler.org/)).

#### About the fork

The purpose of the fork is to bundle the library and its add-ons for convenient loading.

There has been no changes made to the library, since the original is already well-designed as a Composer module. It loads the newest version of itself in case multiple plugins include the library. Notably, WooCommerce uses it.

To sync from source, add the GitHub repo as a remote named `upstream`, and merge to main branch.


## Install

Create or add to the plugin's `composer.json`

```json
{
  "repositories": [{
    "type": "vcs",
    "url": "git@bitbucket.org:/tangibleinc/tangible-action-scheduler-module.git"
  }],
  "require": {
    "tangible/action-scheduler": "dev-master"
  },
  "minimum-stability": "dev"
}
```

Run on command line

```sh
composer install
```

Later, regularly keep module up to date

```sh
composer update
```


## Load

Load the library at the top of the plugin entry file.

```php
require __DIR__ . '/vendor/tangible/action-scheduler/index.php';
```


## Notes before using

- Action Scheduler functions are available after action `init` with priority 1. (See [API Function Availability](https://actionscheduler.org/api/#api-function-availability))

- It is designed to work with single sites, and currently has no features specific to multisite setup. (See [FAQ](https://actionscheduler.org/faq/#how-does-action-scheduler-work-on-wordpress-multisite))

- You can see past and future scheduled events from the admin menu: Tools &gt; Scheduled Actions. If WooCommerce is active, it will also be under: WooCommerce &gt; Status &gt; Scheduled Actions.


## Usage

Please refer to [the API Reference](https://actionscheduler.org/api/).

Here is a minimal example.

```php
$action_name = 'tangible_action_unique_name';

add_action('init', function() use ($action_name) {
  if (as_has_scheduled_action( $action_name )===false) {
    as_schedule_recurring_action(strtotime('tomorrow'), DAY_IN_SECONDS, $action_name);
  }
});

add_action($action_name, function() {

  // Do the thing

});
```

## High volume

For a server setup with large capacity, optionally include the "High Volume" add-on ([source](https://github.com/woocommerce/action-scheduler-high-volume) and [documentation](https://actionscheduler.org/perf/)).

```php
require __DIR__ . '/vendor/tangible/action-scheduler/high-volume.php';
```

This modifies Action Scheduler's configuration, such as increasing time limit (120 seconds), batch size (100 actions per batch), and concurrency (10 queues at a time).


## Using cron and WP-CLI

By default, Action Scheduler depends on [WP-Cron](https://developer.wordpress.org/plugins/cron/) to run the background processing of tasks. This happens inside a web request, which has time and memory limitations; it can only process queues one at a time; and is unpredictable (triggered by user visit, not the actual scheduled time).

To work around these limits, it's possible to use Unix-native [cron](https://en.wikipedia.org/wiki/Cron) to run the queue runner via [WP-CLI](http://wp-cli.org/), outside the context of a web request. For details of this technique, see [High Performance Background Processing with WooCommerce](https://pantheon.io/blog/high-performance-background-processing-woocommerce-pantheon).

---

From the server's shell prompt, run the following to edit the "crontab" (cron table) file. This opens the default editor, usually `nano` or `vim`.

```sh
crontab -e
```

Enter the scheduled command. The following is an example from the afore-mentioned article, which runs the command every minute.

```cron
*/1 * * * * wp action-scheduler run >> ~/logs/action-scheduler.log 2>> ~/logs/action-scheduler-error.log
```

Some points to keep in mind:

- You may need to specify the full path to the `wp` command.
- Change the location of log files as desired, and ensure that the directory exists.
- To change the email address where notification is sent, add a line at the top of the file: `MAILTO=admin@example.com`
- For a list of options supported by Action Scheduler's WP-CLI command, see: https://actionscheduler.org/wp-cli/.

Save and exit the editor. (In `nano`, press CTRL+X and enter. In `vim`, press ESC then `:q`.)

---

After setting up the cron job, load the add-on to disable the default queue runnner.

```php
require __DIR__ . '/vendor/tangible/action-scheduler/disable-default-queue-runner.php';
```

It can be in a site-specific plugin or theme (such as `functions.php`).

In the future, a settings page may be provided for this purpose.
