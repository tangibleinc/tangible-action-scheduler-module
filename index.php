<?php
/**
 * Based on: https://github.com/woocommerce/action-scheduler
 *
 * @see https://actionscheduler.org/api/
 */

require_once __DIR__.'/lib/action-scheduler.php';
